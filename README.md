# Multitenant authorization

### Associate roles to permissions and users to roles per tenant

With this package you can manage users roles and permissions in a multitenant application. Roles and permissions are saved to database. Package is built upon [Laravel's authorization functionality](http://laravel.com/docs/5.3/authorization).

Roles can be either generic or tenant related. Generic roles can't be associated with tenant and on the other hand tenant roles must be. Example of a generic role could be administrator that have permissions to features that are not tenant related.

Package also contains controller traits, views and seeds to make it easy to implement.

With this package you could do something like this:


```php
$user->syncRoles($roles, $organization);
```

```php
$user->syncGenericRoles($roles);
```

You can test if a user has a permission with Laravel's default `can`-function. *Package encourage to use route names as permissions, but you can name them as you wish.*
```php
$user->can('articles.edit');
```
You can use controller traits to make implementation fast and easy.
```php
class PermissionsController extends Controller
{
    // CRUD for your permissions.
    use Permissions;

    // Feel free to overwrite methods or publish views etc.
}
```

## Install

Install the package via composer:
``` bash
$ composer require klopal/laravel-permission-multitenant
```

Add service provider to app.php providers array.
```php
// config/app.php
'providers' => [
    ...
    Klopal\Permissions\PermissionsServiceProvider::class,
];
```

Publish the migration with:
```bash
php artisan vendor:publish --provider="Klopal\Permissions\PermissionServiceProvider" --tag="migrations"
```

The package assumes that your users table name is called "users". If this is not the case
you should manually edit the published migration to use your custom table name.

After the migration has been published you can create the roles ,permissions and organizations tables by
running the migrations:

```bash
php artisan migrate
```

You can publish the config-file with:
```bash
php artisan vendor:publish --provider="Klopal\Permissions\PermissionServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
// config/laravel-permission.php

return [

    /*
    |--------------------------------------------------------------------------
    | Authorization Models
    |--------------------------------------------------------------------------
    */

    'models' => [

        /*
        |--------------------------------------------------------------------------
        | Permission Model
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | Eloquent model should be used to retrieve your permissions. Of course, it
        | is often just the "Permission" model but you may use whatever you like.
        |
        | The model you want to use as a Permission model needs to implement the
        | `Klopal\Permissions\Contracts\Permission` contract.
        |
        */

        'permission' => Klopal\Permissions\Models\Permission::class,

        /*
        |--------------------------------------------------------------------------
        | Role Model
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | Eloquent model should be used to retrieve your roles. Of course, it
        | is often just the "Role" model but you may use whatever you like.
        |
        | The model you want to use as a Role model needs to implement the
        | `Klopal\Permissions\Contracts\Role` contract.
        |
        */

        'role' => Klopal\Permissions\Models\Role::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Authorization Tables
    |--------------------------------------------------------------------------
    */

    'table_names' => [

        /*
        |--------------------------------------------------------------------------
        | Roles Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your roles. We have chosen a basic
        | default value but you may easily change it to any table you like.
        |
        */

        'roles' => 'roles',

        /*
        |--------------------------------------------------------------------------
        | Permissions Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your permissions. We have chosen a basic
        | default value but you may easily change it to any table you like.
        |
        */

        'permissions' => 'permissions',

        /*
        |--------------------------------------------------------------------------
        | User Permissions Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your users permissions. We have chosen a
        | basic default value but you may easily change it to any table you like.
        |
        */

        'user_has_permissions' => 'user_has_permissions',

        /*
        |--------------------------------------------------------------------------
        | User Roles Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your users roles. We have chosen a
        | basic default value but you may easily change it to any table you like.
        |
        */

        'user_has_roles' => 'user_has_roles',

        /*
        |--------------------------------------------------------------------------
        | Role Permissions Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your roles permissions. We have chosen a
        | basic default value but you may easily change it to any table you like.
        |
        */

        'role_has_permissions' => 'role_has_permissions',

    ],

];
```

## Usage

First add the `Klopal\Permissions\Models\Traits\HasRoles`-trait to your User model.

```php
use Klopal\Permissions\Models\Traits\HasRoles;
use Klopal\Permissions\Models\Traits\HasOrganizations;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles, HasOrganizations

    // ...
}
```

This package allows for users to be associated with roles. Permissions can be associated with roles.
A `Role` and a `Permission` are regular Eloquent-models. They can have a name and can be created like this:

```php
use Klopal\Permissions\Models\Role;
use Klopal\Permissions\Models\Permission;

$role = Role::create(['name' => 'writer']);
$permission = Permission::create(['name' => 'articles.create']);
```

The `HasRoles` adds eloquent relationships to your models, which can be accessed directly or used as a base query.

```php
$permissions = $user->permissions;
$roles = $user->roles()->pluck('name'); // returns a collection
```


You can use Laravel's native `@can` directive to check if a user has a certain permission.

## Using a middleware


## Extending

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email [freek@Klopal.be](mailto:freek@Klopal.be) instead of using the issue tracker.

## Credits

## Alternatives

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.