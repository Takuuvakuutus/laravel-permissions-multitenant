# Changelog

This changelog references the relevant changes done to `klopal/laravel-permissions`.

## 0.2.5

Date: 22.02.2017

### Changes

* Fix organization cache flush issue.

## 0.2.4

Date: 16.02.2017

### Changes

* Refactor the way user's permissions are cached.

## 0.2.3

Date: 16.02.2017

### Changes

* Bug fix when admin is saving multiple organization roles at the same time.

## 0.2.2

Date: 14.02.2017

### Changes

* Minor tweaks.

## 0.2.1

Date: 13.02.2017

### Changes

* Added asAdmin method.

## 0.2.0

Date: 10.02.2017

### Changes

* Added caching.

