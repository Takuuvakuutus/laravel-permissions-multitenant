<?php

namespace Klopal\Permissions;

use Illuminate\Routing\Router;

class Routes
{
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @return static
     */
    public function getRoutes()
    {
        return collect($this->router->getRoutes())->map(function ($route) {
            return $route->getName();
        })->reject(function ($name) {
            return empty($name);
        })->unique();
    }

    /**
     * @param $permissions
     * @return mixed
     */
    public function virtualPermissionRoutes($permissions) {
        return $this->getRoutes($permissions)->diff($permissions->pluck('name'));
    }
}