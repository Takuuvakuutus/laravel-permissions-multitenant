<?php

return [
    'save'                         => 'Tallenna',
    'permissions'                  => 'Oikeudet',
    'permission'                   => 'Oikeus',
    'name'                         => 'Nimi',
    'description'                  => 'Kuvaus',
    'related_roles'                => 'Oikeuteen liitetyt roolit',
    'add_user_to_new_organization' => 'Liitä käyttäjä uuteen yritykseen',
    'notice'                       => 'Vältä luomasta oikeuksia joiden nimi muistuttaa mahdollista reitin nimeä. Luomalla oikeuden ja myöhemmin saman nimisen reitin, saa oikeuden omaavat käyttäjät suoraan oikeuden juuri luotuun reittiin. Etuliitteen käyttö oikeuksien nimessä ratkaisee ongelman.',
    'permissions_count'            => 'Oikeuksia yhteensä',
    'routes_count'                 => 'Virtuaalioikeuksia yhteensä',
    'virtual_permission'           => 'Virtuaalioikeus',
    'create_new'                   => 'Luo uusi oikeus',
    'edit'                         => 'Muokkaa',
];
