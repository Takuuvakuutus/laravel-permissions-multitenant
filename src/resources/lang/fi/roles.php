<?php

return [
    'save'                      => 'Tallenna',
    'delete'                    => 'Poista',
    'roles'                     => 'Roolit',
    'role'                      => 'Rooli',
    'roles_in_organization'     => 'Roolit organisaatiossa ',
    'generic_roles'             => 'Järjestelmän laajuiset roolit',
    'slug'                      => 'Esitys nimi',
    'name'                      => 'Nimi',
    'description'               => 'Kuvaus',
    'user_has_no_roles'         => 'Käyttäjällä ei ole rooleja',
    'user_has_no_generic_roles' => 'Käyttäjällä ei ole järjestelmän laajuisia rooleja',
    'slug_help'                 => '',
    'created_at'                => 'Luotu',
    'updated_at'                => 'Muokattu',
    'create_role'               => 'Luo uusi rooli',
    'edit'                      => 'Muokkaa',
    'generic'                   => 'Järjestelmän laajuinen',
];
