<?php

return [

    'exist_in_routes' => 'Oikeutta ei voida lisätä koska saman niminen reitti (virtuaalioikeus) on jo olemassa.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [

    ],

];
