@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Käyttäjän roolit</h5>
                </div>
                <div class="ibox-content">
                    {{ Form::open(['route' => ['users.roles.store', $user]]) }}

                    {{ Form::kselect(trans('common.organization'), 'organization_id', $organizations->pluck('name', 'id'), $organization ?:null) }}

                    @foreach($roles as $role)
                        {{ Form::kcheckbox($role->slug, 'roles[]', $role->name) }}
                    @endforeach

                    <hr />

                    @btnsubmit

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
@endsection