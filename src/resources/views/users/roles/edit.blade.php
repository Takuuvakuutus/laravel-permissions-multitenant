@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Käyttäjän organisaatiot ja roolit niissä</h5>
                </div>
                <div class="ibox-content">

                    {{ Form::kopen('users.roles.organizations.update', $user ,['method' => 'put']) }}

                    @if(count($user->organizations))
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="col-sm-4">Organisaatio</th>
                                <th>Roolit</th>
                            </tr>
                            </thead>
                            @foreach($user->organizations as $organization)
                                <tr>
                                    <td>{{ $organization->name }}</td>
                                    <td>
                                        @foreach($roles as $role)
                                            {{ Form::kcheckbox(
                                            $role->slug,
                                            'organizationRoles['.$organization->id.'][]',
                                            $role->name,
                                            $user->hasRoleInOrganization($role, $organization->id)) }}
                                        @endforeach

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <i>Käyttäjä ei kuulu yhteenkään organisaatioon.</i>
                    @endif

                    <hr/>

                    @btn

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
@endsection