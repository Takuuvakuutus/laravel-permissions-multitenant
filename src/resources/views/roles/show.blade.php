@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ $role->slug }}</h5>
                </div>
                <div class="ibox-content">

                    @include('permissions::roles.partials.show')

                    <hr/>

                    <a href="{{ route('roles.edit', $role) }}" class="btn btn-w-m btn-default">
                        <i class="fa fa-fw fa-pencil"></i> @lang('permissions::roles.edit')
                    </a>

                </div>
            </div>
        </div>
    </div>
@endsection