@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        @lang('permissions::roles.roles')
                    </h5>
                    @can('roles.create')
                        <div class="ibox-tools">
                            <a href="{{ route('roles.create') }}" class="btn btn-primary btn-outline btn-xs">
                                <i class="fa fa-fw fa-plus" aria-hidden="true"></i> @lang('permissions::roles.create_role')
                            </a>
                        </div>
                    @endcan
                </div>
                <div class="ibox-content">

                    @include('permissions::roles.partials.list')

                </div>
            </div>
        </div>
    </div>
@endsection