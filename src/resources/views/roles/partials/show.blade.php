<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('permissions::roles.name')
    </div>
    <div class="col-sm-8 col-md-10">
        {{ $role->name }}
    </div>
</div>

@hr

<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('permissions::roles.slug')
    </div>
    <div class="col-sm-8 col-md-10">
        {{ $role->slug }}
    </div>
</div>

@hr

<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('permissions::roles.description')
    </div>
    <div class="col-sm-8 col-md-10">
        {{ $role->description }}
    </div>
</div>

@hr

<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('permissions::permissions.permissions')
    </div>
    <div class="col-sm-8 col-md-10">

        @forelse($role->permissions as $permission)
            {{ $permission->name }}@if($loop->remaining), @endif
        @empty

        @endforelse
    </div>
</div>

@hr

<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('permissions::roles.created_at')
    </div>
    <div class="col-sm-8 col-md-10">
        {{ $role->created_at }}
    </div>
</div>

@hr

<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('permissions::roles.updated_at')
    </div>
    <div class="col-sm-8 col-md-10">
        {{ $role->updated_at }}
    </div>
</div>