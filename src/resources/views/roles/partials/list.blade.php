<table class="table table-hover">
    <thead>
    <tr>
        <th class="narrow">#</th>
        <th>{{ trans('permissions::roles.name') }}</th>
        <th>{{ trans('permissions::roles.slug') }}</th>
        <th>{{ trans('permissions::roles.description') }}</th>
        <th class="text-right"></th>
    </tr>
    </thead>
    <tbody>
    @foreach ($roles as $role)
        <tr>
            <td>{{ $role->id }}</td>
            <td>{{ $role->name }}</td>
            <td>{{ $role->slug }}</td>
            <td>{{ $role->description }}</td>

            <td class="narrow text-nowrap text-right">

                <form method="post" action="{{ route('roles.destroy', $role) }}">

                    <a class="btn btn-sm btn-default"
                       href="{{ route('roles.show', $role) }}">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </a>

                    <a class="btn btn-sm btn-default"
                       href="{{ route('roles.edit', $role) }}">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>

                    <!-- CSRF Token -->
                    {!! csrf_field() !!}

                    {!! method_field('delete') !!}

                    <button class="btn btn-sm btn-danger btn-outline">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>