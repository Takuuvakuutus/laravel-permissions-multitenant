<form method="POST" action="{{ $role->exists ? route('roles.update', $role) : route('roles.store') }}"
      accept-charset="UTF-8">
    @if($role->exists)<input name="_method" type="hidden" value="PUT">@endif
    <input name="_token" type="hidden" value="{{ csrf_token() }}">

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->first('name', ' has-error') }}">
                <label for="name" class="control-label">@lang('permissions::roles.name')</label>
                <input class="form-control" name="name" type="text" id="name" value="{{ old('name', $role->name) }}">
                <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                @if(!$errors->first('name'))<span class="help-block"></span>@endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group{{ $errors->first('slug', ' has-error') }}">
                <label for="slug" class="control-label">@lang('permissions::roles.slug')</label>
                <input class="form-control" name="slug" type="text" id="slug" value="{{ old('slug', $role->slug) }}">
                <span class="help-block">{{ $errors->first('slug', ':message') }}</span>
                @if(!$errors->first('slug'))<span class="help-block">@lang('permissions::roles.slug_help')</span>@endif
            </div>
        </div>
    </div>

    <div class="form-group{{ $errors->first('description', ' has-error') }}">
        <label for="description" class="control-label">@lang('permissions::roles.description')</label>
        <textarea class="form-control" name="description" cols="50" rows="5"
                  id="description">{{ old('description', $role->description) }}</textarea>
        <span class="help-block">{{ $errors->first('description', ':message') }}</span>
    </div>

        <div class="form-group{{ $errors->first('generic', ' has-error') }}">
            <label for="description" class="control-label">@lang('permissions::roles.generic')</label>

            <input name="generic" type="hidden" value="0">

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="generic" value="0"> @lang('permissions::roles.generic')
                </label>
            </div>

            <span class="help-block">{{ $errors->first('generic', ':message') }}</span>
        </div>

    <hr/>

    <label>@lang('permissions.permissions')</label>

    <table class="table table-hover table-condensed">
        <thead>
        <tr>
            <th class="col-sm-1"></th>
            <th class="col-sm-3">{{ trans('permissions::roles.name') }}</th>
            <th class="col-sm-7">{{ trans('permissions::roles.description') }}</th>
            <th class="text-right col-sm-1"></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($permissions as $key => $permission)
            <tr>
                <td>
                    <input type="checkbox" id="p{{ $key }}" name="permissions[{{ $key }}]"
                           value="{{ $permission->name }}"
                    @if(!empty(old('permissions')))
                        {{ in_array($permission->name, old('permissions')) ? 'checked ' : '' }}
                            @else
                        {{ ($role->exists && $role->permissions->contains($permission)) ? 'checked' : '' }}
                            @endif
                    />
                </td>
                <td>
                    <label for="p{{ $key }}" style="display:block">
                        {{ $permission->name }}
                    </label>
                </td>
                <td>{{ $permission->description }}</td>
                <td class="text-right"></td>
            </tr>
        @endforeach

        @foreach ($routes as $key => $route)
            <tr>
                <td>
                    <input type="checkbox" id="r{{ $key }}" name="permissions[]" value="{{ $route }}"
                    @if(!empty(old('permissions')))
                        {{ in_array($route, old('permissions')) ? 'checked ' : '' }}
                            @endif
                    />
                </td>
                <td>
                    <label class="text-muted" for="r{{ $key }}" style="display:block">
                        {{ $route }}
                    </label>
                </td>
                <td></td>
                <td class="text-right">
                    <span class="label label-default">Vitual</span>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

    <hr/>

    <button class="btn btn-w-m btn-primary" type="submit">
        <i class="fa fa-fw fa-check"></i> @lang('permissions::roles.save')
    </button>
</form>

@if($role->exists)
    <form method="POST" action="{{ route('roles.destroy', $role) }}" accept-charset="UTF-8">
        <input name="_method" type="hidden" value="DELETE">
        <input name="_token" type="hidden" value="{{ csrf_token() }}">

        <button class="btn btn-w-m btn-danger" type="submit">
            <i class="fa fa-fw fa-trash"></i> @lang('permissions::roles.delete')
        </button>
    </form>
@endif