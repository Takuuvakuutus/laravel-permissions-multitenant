@can('users.roles.generic.update')
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Käyttäjän järjestelmänlaajuiset roolit
                </h5>
            </div>
            <div class="ibox-content">

                {{ Form::kopen('users.roles.generic.update', $user ,['method' => 'put']) }}

                @foreach($genericRoles as $role)
                    {{ Form::kcheckbox($role->slug, 'genericroles[]', $role->name, in_array($role->name, $user->genericroleslist)) }}
                @endforeach

                <hr/>

                <div class="text-right">
                    @btn
                </div>

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@endcan
