@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        @lang('permissions::permissions.permissions')
                    </h5>
                    @can('permissions.create')
                        <div class="ibox-tools">
                            <a href="{{ route('permissions.create') }}" class="btn btn-primary btn-outline btn-xs">
                                <i class="fa fa-fw fa-plus" aria-hidden="true"></i> @lang('permissions::permissions.create_new')
                            </a>
                        </div>
                    @endcan
                </div>
                <div class="ibox-content">

                    @include('permissions::permissions.partials.index')

                </div>
            </div>
        </div>
    </div>
@endsection