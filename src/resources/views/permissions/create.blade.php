@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>@lang('permissions::permissions.create_new')</h5>
                </div>
                <div class="ibox-content">

                    <p class="text-danger">{{ trans('permissions::permissions.notice') }}</p>

                    @include('permissions::permissions.partials.form')

                </div>
            </div>
        </div>
    </div>
@endsection