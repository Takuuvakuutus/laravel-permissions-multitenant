@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ $permission->name }}</h5>
                </div>
                <div class="ibox-content">

                    @include('permissions::permissions.partials.form')

                </div>
            </div>
        </div>
    </div>
@endsection