<table class="table table-bordered">
    <tr>
        <td class="col-sm-3">@lang('permissions::permissions.permissions_count')</td>
        <td class="col-sm-3">{{ count($permissions) }}</td>
        <td class="col-sm-3">@lang('permissions::permissions.routes_count')</td>
        <td class="col-sm-3">{{ count($routes) }}</td>
    </tr>
</table>

<hr />

<table class="table table-condensed table-hover">
    <thead>
    <th class="col-sm-3">@lang('permissions::permissions.name')</th>
    <th class="col-sm-7">@lang('permissions::permissions.description')</th>
    <th class="col-sm-2"></th>
    </thead>
    <tbody>
    @foreach ($permissions as $permission)
        <tr>
            <td>{{ $permission->name }}</td>
            <td>{{ $permission->description }}</td>
            <td class="text-right">
                <a class="btn btn-sm btn-default"
                   href="{{ route('permissions.show', $permission) }}">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </a>
        </tr>
    @endforeach
    @foreach ($routes as $route)
        @if(!empty($route))
            <tr>
                <td>{{ $route }}</td>
                <td></td>
                <td class="text-right">
                    <button class="btn btn-sm btn-default" href="#" disabled>
                        @lang('permissions::permissions.virtual_permission')
                    </button></td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>