<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('permissions.name')
    </div>
    <div class="col-sm-8 col-md-10">
        {{ $permission->name }}
    </div>
</div>

@hr

<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('permissions.description')
    </div>
    <div class="col-sm-8 col-md-10">
        {{ $permission->description }}
    </div>
</div>

@hr

<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('permissions.related_roles')
    </div>
    <div class="col-sm-8 col-md-10">

        @forelse($permission->roles as $role)
            {{ $role->slug }}@if($loop->remaining), @endif
        @empty

        @endforelse
    </div>
</div>

@hr

<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('common.created_at')
    </div>
    <div class="col-sm-8 col-md-10">
        {{ $permission->created_at }}
    </div>
</div>

@hr

<div class="row">
    <div class="col-sm-4 col-md-2">
        @lang('common.updated_at')
    </div>
    <div class="col-sm-8 col-md-10">
        {{ $permission->updated_at }}
    </div>
</div>