<form method="POST"
      action="{{ $permission->exists ? route('permissions.update', $permission) : route('permissions.store') }}"
      accept-charset="UTF-8">
    @if($permission->exists)<input name="_method" type="hidden" value="PUT">@endif
    <input name="_token" type="hidden" value="{{ csrf_token() }}">

    <div class="form-group{{ $errors->first('name', ' has-error') }}">
        <label for="name" class="control-label">@lang('permissions::permissions.name')</label>
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', $permission->name) }}">
        <span class="help-block">{{ $errors->first('name', ':message') }}</span>
        @if(!$errors->first('name'))<span class="help-block"></span>@endif
    </div>

    <div class="form-group{{ $errors->first('description', ' has-error') }}">
        <label for="description" class="control-label">@lang('permissions::permissions.description')</label>
        <textarea class="form-control" name="description" cols="50" rows="5"
                  id="description">{{ old('description', $permission->description) }}</textarea>
        <span class="help-block">{{ $errors->first('description', ':message') }}</span>
    </div>

    <hr/>

    <button class="btn btn-w-m btn-primary" type="submit"><i
                class="fa fa-fw fa-check"></i>@lang('permissions::permissions.save')</button>
</form>