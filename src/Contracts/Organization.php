<?php

namespace Klopal\Permissions\Contracts;

interface Organization
{
    /**
     * A organization has many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users();

    /**
     * Find a organization by its name.
     *
     * @param string $name
     *
     * @throws PermissionDoesNotExist
     */
    public static function findByName($name);
}