<?php

namespace Klopal\Permissions;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Klopal\Permissions\Contracts\Role as RoleContract;
use Klopal\Permissions\Contracts\Permission as PermissionContract;
use Klopal\Permissions\Contracts\Organization as OrganizationContract;

class PermissionsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param PermissionsRegistrar $permissionLoader
     */
    public function boot(PermissionsRegistrar $permissionLoader)
    {
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'permissions');

        $this->loadViewsFrom(__DIR__.'/resources/views', 'permissions');

        /**
         * Publish configuration files.
         */
        $this->publishes([
            __DIR__ . '/../resources/config/klopal-permissions.php' => $this->app->configPath() . '/' . 'klopal-permissions.php',
        ], 'config');

        /**
         * Publish views.
         */
        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/permissions'),
        ], 'views');

        /**
         * Publish localization files.
         */
        $this->publishes([
            __DIR__.'/resources/lang' => resource_path('lang/vendor/permissions'),
        ], 'lang');

        /**
         * Publish localization files.
         */
        $this->publishes([
            __DIR__.'/../resources/seeds' => $this->app->databasePath() .'/seeds',
        ], 'seed');

        /**
         * Publish migrations.
         */
        if (!class_exists('CreatePermissionsTables')) {
            $timestamp = date('Y_m_d_His', time());
            $this->publishes([
                __DIR__ . '/../resources/migrations/create_permissions_tables.php.stub' => $this->app->databasePath() . '/migrations/' . $timestamp . '_create_permissions_tables.php',
            ], 'migrations');
        }

        if (!class_exists('CreateOrganizationsTables') and config('klopal-permissions.multi_tenant') === true) {
            $timestamp = date('Y_m_d_His', time() + 1);
            $this->publishes([
                __DIR__ . '/../resources/migrations/create_organizations_tables.php.stub' => $this->app->databasePath() . '/migrations/' . $timestamp . '_create_organizations_tables.php',
            ], 'migrations');
        }

        $permissionLoader->registerPermissions();
    }


    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../resources/config/klopal-permissions.php', 'klopal-permissions');

        $this->registerModelBindings();

        $this->registerBladeExtensions();
    }

    /**
     * Bind the Permission, Role and Organization model into the IoC.
     */
    protected function registerModelBindings()
    {
        $config = $this->app->config['klopal-permissions.models'];

        $this->app->bind(PermissionContract::class, $config['permission']);
        $this->app->bind(RoleContract::class, $config['role']);
        $this->app->bind(OrganizationContract::class, $config['organization']);
    }

    /**
     * Register the blade extensions.
     */
    protected function registerBladeExtensions()
    {
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {

            $bladeCompiler->directive('hasrole', function ($role) {
                return "<?php if(auth()->check() && auth()->user()->hasRoleInOrganization({$role}, auth()->user()->current_organization_id )): ?>";
            });
            $bladeCompiler->directive('endhasrole', function () {
                return '<?php endif; ?>';
            });

            $bladeCompiler->directive('hasorganizations', function () {
                return "<?php if(auth()->check() && auth()->user()->hasOrganizations()): ?>";
            });
            $bladeCompiler->directive('endhasorganizations', function () {
                return '<?php endif; ?>';
            });
        });
    }
}
