<?php

namespace Klopal\Permissions\Exceptions;

use Exception;

class RoleDoesNotExist extends Exception
{
}
