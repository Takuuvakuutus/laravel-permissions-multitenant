<?php

namespace Klopal\Permissions\Exceptions;

use Exception;

class OrganizationDoesNotExist extends Exception
{
}
