<?php
namespace Klopal\Permissions\Exceptions;

use DomainException;

class PermissionMustNotBeEmpty extends DomainException
{

}