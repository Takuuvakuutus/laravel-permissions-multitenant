<?php

namespace Klopal\Permissions\Exceptions;

use Exception;

class UserDoesNotBelongToAnyOrganization extends Exception
{
}
