<?php

namespace Klopal\Permissions\Exceptions;

use Exception;

class PermissionDoesNotExist extends Exception
{
    public static function doesNotExist($name)
    {
        return new static('Permission "' . $name . '" does not exist in database.');
    }
}