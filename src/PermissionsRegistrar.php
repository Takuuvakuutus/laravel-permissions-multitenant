<?php

namespace Klopal\Permissions;

use Log;
use Exception;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Cache\Repository;
use Klopal\Permissions\Contracts\Permission;

class PermissionsRegistrar
{
    /**
     * @var Gate
     */
    protected $gate;

    /**
     * @var Repository
     */
    protected $cache;

    /**
     * @var string
     */
    protected $cacheKey = 'klopal.permissions.permissions';


    /**
     * @param Gate $gate
     */
    public function __construct(Gate $gate, Repository $cache)
    {
        $this->gate = $gate;
        $this->cache = $cache;
    }

    /**
     *  Register the permissions.
     *
     * @return bool
     */
    public function registerPermissions()
    {
        try {
            // Loop trough all permissions.
            $this->getPermissions()->map(function ($permission) {

                // By pass all authorization checks for developers.
                $this->gate->before(function ($user, $ability) {
                    if (in_array($user->email, config('klopal-permissions.developers'))) {
                        return true;
                    }
                });

                // Register permissions for user.
               $this->gate->define($permission->name, function ($user) use ($permission) {
                    return $user->hasPermissionTo($permission);
                });
            });

            return true;
        } catch (Exception $e) {
            Log::alert(
                "Could not register permissions because {$e->getMessage()}".PHP_EOL
                .$e->getTraceAsString()
            );

            return false;
        }
    }

    /**
     *  Forget the cached permissions.
     */
    public function forgetCachedPermissions()
    {
        $this->cache->forget($this->cacheKey);
    }

    /**
     * Get the current permissions.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getPermissions()
    {
        return $this->cache->rememberForever($this->cacheKey, function () {
            return app(Permission::class)->with('roles')->get();
        });
    }
}
