<?php

Route::resource('roles', 'RolesController');
Route::resource('permissions', 'PermissionsController');
Route::resource('users.roles', 'UsersRolesController', ['only' => ['index', 'create', 'store']]);

Route::put('users/{user}/roles', ['as' => 'users.roles.update', 'uses' => 'UsersRolesController@update']);
Route::put('users/{user}/update-organization-roles', ['as' => 'users.roles.update-organization-roles', 'uses' => 'UsersRolesController@updateOrganizationRoles']);
Route::put('users/{user}/update-generic-roles', ['as' => 'users.roles.update-generic-roles', 'uses' => 'UsersRolesController@updateGenericRoles']);
