<?php

namespace Klopal\Permissions\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Klopal\Permissions\Routes;
use Klopal\Permissions\Contracts\Permission;

trait Permissions
{
    /**
     * @param Request $request
     * @param Router $router
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Router $router, Routes $routes)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $permissions = app(Permission::class)->all();

        $routes = $routes->virtualPermissionRoutes($permissions);

        return view('permissions::permissions.index', compact('permissions', 'routes'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request, Router $router)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $permission = app(Permission::class);

        return view('permissions::permissions.create', compact('permission'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Router $router)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $existingRoutes = collect($router->getRoutes())->map(function ($route) {
            return $route->getName();
        })->implode(',');

        // Given name can not been found in database neither from routes.
        $this->validate($request, [
            'name' => 'required|max:255|unique:permissions,name|not_in:'. $existingRoutes,
        ],[
            'not_in' => trans('permissions::validation.exist_in_routes'),
        ]);

        app(Permission::class)->create($request->all());

        return redirect()->route('permissions.index');
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $permission
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, Router $router, $permission)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $permission = app(Permission::class)->findOrFail($permission);

        return view('permissions::permissions.show', compact('permission'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param Permission $permission
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, Router $router, $permission)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $permission = app(Permission::class)->findOrFail($permission);

        return view('permissions::permissions.edit', compact('permission'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $permission
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Router $router, $permission)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $permission = app(Permission::class)->findOrFail($permission);

        $this->validate($request, [
            'name' => 'required|max:255|unique:permissions,name,' . $permission->id,
        ]);

        $permission->update($request->all());

        return redirect()->route('permissions.index');
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $permission
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Router $router, $permission)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $permission = app(Permission::class)->findOrFail($permission);

        $permission->delete();

        return redirect()->route('permissions.index');
    }
}
