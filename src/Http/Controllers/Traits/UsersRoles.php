<?php

namespace Klopal\Permissions\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Klopal\Permissions\Contracts\Role;
use Klopal\Permissions\Contracts\Organization;

trait UsersRoles
{
    /**
     * @param Request $request
     * @param Router $router
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Router $router, $user)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $roles = app(Role::class)->where('generic', false)->get();

        return view('permissions::users.roles.edit', compact('user', 'roles'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request, Router $router, $user)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $roles = app(Role::class)->where('generic', false)->get();

        $organizations = app(Organization::class)->all()->diff($user->organizations);

        // If user is already in all organizations redirect back to profile.
        if (empty($organizations)) {
            return redirect()->route('users.show', $user);
        }

        // To preselect organization in dropdown if session contains the value.
        $organization = session('organization');

        return view('permissions::users.roles.create', compact('user', 'roles', 'organizations', 'organization'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Router $router, $user)
    {
        $organizationId = $request->get('organization_id');

        abort_unless($request->user()->can($router->currentRouteName()) && currentUser()->onOrganization($organizationId), 403);

        $user->syncRoles($request->get('roles', []), $organizationId);

        return redirect()->route('users.show', $user);
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Router $router, $user)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $roles = $request->get('roles', []);

        $user->syncRoles($roles, currentUser()->current_organization_id);

        return redirect()->route('users.index');
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateOrganizationRoles(Request $request, Router $router, $user)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $roles = $request->get('organizationRoles', []);

        // If no single organization role is given remove all of them.
        if(empty($roles)) {
            $user->roles()->detach(
                $user->roles()->pluck('id')->all()
            );

            $user->current_organization_id = null;
            $user->save();

            $user->forgetUserCache();
        }

        foreach ($user->organizations as $organization) {
            $organizationRoles = isset($roles[$organization->id]) ? $roles[$organization->id] : [];
            $user->syncRoles($organizationRoles, $organization->id);
        }

        return redirect()->route('users.show', $user);
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateGenericRoles(Request $request, Router $router, $user)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $user->syncGenericRoles($request->get('genericroles', []));

        return redirect()->route('users.show', $user);
    }
}
