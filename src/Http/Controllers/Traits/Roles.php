<?php

namespace Klopal\Permissions\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Klopal\Permissions\Contracts\Role;
use Klopal\Permissions\Contracts\Permission;
use Klopal\Permissions\Routes;

trait Roles
{
    /**
     * @param Request $request
     * @param Router $router
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Router $router)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $roles = app(Role::class)->all();

        return view('permissions::roles.index', compact('roles'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param Routes $routes
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request, Router $router, Routes $routes)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $permissions = app(Permission::class)->all();

        $routes = $routes->virtualPermissionRoutes($permissions);

        $role = app(Role::class);

        return view('permissions::roles.create', compact('permissions', 'routes', 'role'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Router $router)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $this->validate($request, [
            'name' => 'required|max:255|unique:roles,name',
            'slug' => 'required|max:255|unique:roles,slug',
        ]);

        $role = app(Role::class)->create($request->all());

        $role->syncPermissions($request->get('permissions', []));

        return redirect()->route('roles.index');
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, Router $router, $role)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $role = app(Role::class)->findOrFail($role);

        return view('permissions::roles.show', compact('role'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, Router $router, Routes $routes, $role)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $role = app(Role::class)->findOrFail($role);

        $permissions = app(Permission::class)->all();

        $routes = $routes->virtualPermissionRoutes($permissions);

        return view('permissions::roles.edit', compact('role', 'permissions', 'routes'));
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Router $router, $role)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $role = app(Role::class)->findOrFail($role);

        $this->validate($request, [
            'name' => 'required|max:255|unique:roles,name,' . $role->id,
            'slug' => 'required|max:255|unique:roles,slug,' . $role->id,
        ]);

        $role->update($request->all());

        $role->syncPermissions($request->get('permissions', []));

        return redirect()->route('roles.index');
    }

    /**
     * @param Request $request
     * @param Router $router
     * @param $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Router $router, $role)
    {
        abort_unless($request->user()->can($router->currentRouteName()), 403);

        $role = app(Role::class)->findOrFail($role);

        $relatedUsers = $role->users;

        // Do not delete role if there is users connected. Maybe allow it later..
        if (empty($relatedUsers)) {
            $role->delete();
        }

        return redirect()->route('roles.index');
    }
}
