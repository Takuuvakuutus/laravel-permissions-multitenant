<?php

namespace Klopal\Permissions\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Klopal\Permissions\Contracts\Organization as OrganizationContract;

class Organization extends Model implements OrganizationContract
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * @var array
     */
    public $guarded = ['id'];

    /**
     * Organization constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('klopal-permissions.table_names.organizations'));
    }

    /**
     * Users are related to organizations via roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(config('auth.providers.users.model'), 'role_user', 'organization_id', 'user_id');
    }

    /**
     * Find a organization by its name.
     *
     * @param $name
     * @return mixed
     * @throws OrganizationDoesNotExist
     */
    public static function findByName($name)
    {
        $organization = static::where('name', $name)->first();

        if (!$organization) {
            throw new OrganizationDoesNotExist();
        }

        return $organization;
    }

    /**
     * Flush users cached organizations.
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function(){
            Log::info('Deleting organization and related user cache...');

            Cache::tags('organizations')->flush();
        });
    }
}
