<?php

namespace Klopal\Permissions\Models;

use Illuminate\Database\Eloquent\Model;
use Klopal\Permissions\Exceptions\RoleDoesNotExist;
use Klopal\Permissions\Models\Traits\HasPermissions;
use Klopal\Permissions\Contracts\Role as RoleContract;
use Klopal\Permissions\Models\Traits\RefreshesPermissionCache;

class Role extends Model implements RoleContract
{
    use HasPermissions;

    // When ever permission is stored, updated, deleted we flush related cache.
    use RefreshesPermissionCache;

    /**
     * @var array
     */
    public $fillable = ['name', 'slug', 'description', 'generic'];

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('klopal-permissions.table_names.roles'));
    }

    /**
     * A role may be assigned to various users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            config('auth.model') ?: config('auth.providers.users.model'),
            config('klopal-permissions.table_names.user_role')
        )->withTimestamps();
    }

    /**
     * A role may be given various permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(
            config('klopal-permissions.models.permission'),
            config('klopal-permissions.table_names.permission_role')
        )->withTimestamps();
    }

    /**
     * Find a role by its name.
     *
     * @param string $name
     *
     * @return Role
     *
     * @throws RoleDoesNotExist
     */
    public static function findByName($name)
    {
        $role = static::where('name', $name)->first();

        if (!$role) {
            throw new RoleDoesNotExist($name);
        }

        return $role;
    }
}
