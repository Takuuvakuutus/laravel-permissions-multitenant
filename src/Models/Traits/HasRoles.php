<?php

namespace Klopal\Permissions\Models\Traits;

use Klopal\Permissions\Contracts\Role;
use Klopal\Permissions\Contracts\Permission;

trait HasRoles
{
    use Cache;
    use RefreshesPermissionCache;

    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(
            config('klopal-permissions.models.role'),
            config('klopal-permissions.table_names.role_user'))
            ->withPivot('organization_id', 'role_id')
            ->where('roles.generic', false)
            ->withTimestamps();
    }

    /**
     *  User's generic roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function genericRoles()
    {
        return $this->belongsToMany(
            config('klopal-permissions.models.role'),
            config('klopal-permissions.table_names.role_user'))
            ->where('roles.generic', true)
            ->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->cache('klopal.permissions.user.' . $this->id . '.roles', $this->roles());
    }

    /**
     * @return mixed
     */
    public function getGenericRoles()
    {
        return $this->cache('klopal.permissions.user.' . $this->id . '.roles.generic', $this->genericRoles());
    }

    /**
     * Synchronize roles for user.
     *
     * @param array $roles
     * @param null $organizationId
     * @return $this
     */
    public function syncRoles(array $roles, $organizationId = null)
    {
        $this->forgetUserCache();

        return $this->assignRoles($roles, $organizationId);
    }

    /**
     * @param array $roles
     * @param $organizationId
     * @return $this
     */
    protected function assignRoles(array $roles, $organizationId)
    {
        $organizationId = $this->getStoredOrganization(intval($organizationId))->id;

        $userRoles = $this->roles()->wherePivot('organization_id', $organizationId)->pluck('id')->all();
        if (!empty($userRoles)) {
            $this->roles()->detach($userRoles);
        }

        collect($roles)
            ->flatten()
            ->map(function ($role) {
                return $this->getStoredRole($role);
            })
            ->each(function ($item) use ($organizationId) {
                $this->roles()->attach($item->id, ['organization_id' => $organizationId]);
            });

        return $this;
    }

    /**
     * Synchronize generic roles for user.
     *
     * @param array $roles
     * @return $this
     */
    public function syncGenericRoles(array $roles)
    {
        $this->forgetUserCache();

        $userGenericRoles = $this->genericRoles()->pluck('id')->all();
        if (!empty($userGenericRoles)) {
            $this->genericRoles()->detach($userGenericRoles);
        }

        collect($roles)
            ->flatten()
            ->map(function ($role) {
                return $this->getStoredRole($role);
            })
            ->reject(function ($name) {
                return empty($name);
            })
            ->each(function ($item) {
                $this->roles()->attach($item->id);
            });

        return $this;
    }

    /**
     * Remove the given role from the user.
     *
     * @param string|Role $role
     */
    public function removeRole($role)
    {
        $this->forgetUserCache();

        $this->roles()->detach($this->getStoredRole($role));
    }

    /**
     * Determine if the user has (one of) the given role(s).
     *
     * @param string|array|Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasRole($roles)
    {
        if (is_string($roles)) {
            return $this->getRoles()->contains('name', $roles);
        }

        if ($roles instanceof Role) {
            return $this->getRoles()->contains('id', $roles->id);
        }

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }

            return false;
        }

        return (bool)$roles->intersect($this->roles)->count();
    }

    /**
     * Determine if the user has (one of) the given role(s).
     *
     * @param string|array|Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasGenericRole($roles)
    {
        if (is_string($roles)) {
            return $this->getGenericRoles()->contains('name', $roles);
        }

        if ($roles instanceof Role) {
            return $this->getGenericRoles()->contains('id', $roles->id);
        }

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->genericRoles($role)->first()) {
                    return true;
                }
            }

            return false;
        }

        return (bool)$this->getGenericRoles()->intersect($roles)->count();
    }

    /**
     * Determine if the user has (one of) the given role(s) in current organization.
     *
     * @param $roles
     * @param $organizationId
     * @return bool
     */
    public function hasRoleInOrganization($roles, $organizationId)
    {
        if (is_string($roles)) {
            return $this->userRolesInOrganization($organizationId)->contains('name', $roles);
        }

        if ($roles instanceof Role) {
            return $this->userRolesInOrganization($organizationId)->contains('id', $roles->id);
        }

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRoleInOrganization($role, $organizationId)) {
                    return true;
                }
            }

            return false;
        }
        return (bool)$this->userRolesInOrganization($organizationId)
            ->intersect($roles)
            ->count();
    }

    /**
     * @param $organizationId
     * @return mixeds
     */
    protected function userRolesInOrganization($organizationId)
    {
        return $this->cache(
            'klopal.permissions.user.' . $this->id . '.roles.organizations.' . $organizationId,
            $this->roles()->wherePivot('organization_id', '=', $organizationId)
        );
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param string|Permission $permission
     *
     * @return bool
     */
    public function hasPermissionTo($permission)
    {
        if (is_string($permission)) {
            $permission = app(Permission::class)->findByName($permission);

            if (!$permission) {
                return false;
            }
        }

        return $this->hasPermissionViaRole($permission);
    }

    /**
     * Determine if the user has, via roles, the given permission.
     *
     * @param Permission $permission
     * @return bool
     */
    protected function hasPermissionViaRole(Permission $permission)
    {
        // First look if user has permission via generic role.
        if ($this->hasGenericRole($permission->roles)) {
            return true;
        }

        return $this->hasRoleInOrganization($permission->roles, $this->oid());
    }

    /**
     * @param $role
     * @return mixed
     */
    protected function getStoredRole($role)
    {
        if (is_string($role)) {
            $role = app(Role::class)->findByName($role);
        }

        return $role;
    }

    /**
     * @return mixed
     */
    public function getRolesListAttribute()
    {
        return $this->roles()->get()->pluck('name')->toArray();
    }

    /**
     * @return mixed
     */
    public function getGenericRolesListAttribute()
    {
        return $this->genericRoles()->get()->pluck('name')->toArray();
    }

    /**
     * Get user's organization id if multi-tenancy is set true.
     *
     * @return null
     */
    protected function oid()
    {
        return $this->currentOrganization ? $this->currentOrganization->id : null;
    }

    /**
     * If user has "admin" role and no organization selected he is using system as admin.
     *
     * @return bool
     */
    public function asAdmin() {
        return ($this->currentOrganization === null && $this->hasGenericRole(config('klopal-permissions.admin_role')));
    }

    /**
     * If user has "admin" role and no organization selected he is using system as admin.
     *
     * @return bool
     */
    public function asHandler() {
        return ($this->currentOrganization === null && $this->hasGenericRole('claim-handler'));
    }
}
