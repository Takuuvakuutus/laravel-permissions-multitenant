<?php

namespace Klopal\Permissions\Models\Traits;

use Illuminate\Support\Facades\Route;
use Klopal\Permissions\Contracts\Permission;

trait HasPermissions
{
    /**
     * Grant the given permission(s) to a role.
     *
     * @param string|array|Permission|\Illuminate\Support\Collection $permissions
     *
     * @return HasPermissions
     */
    protected function givePermissionTo(array $permissions)
    {
        $permissions = collect($permissions)
            ->flatten()
            ->map(function ($permission) {
                return $this->getStoredPermission($permission);
            })
            ->reject(function ($name) {
                return empty($name);
            })
            ->pluck('id')
            ->all();

        $this->permissions()->attach($permissions);

        return $this;
    }

    /**
     * Remove all current permissions and set the given ones.
     *
     * @param array $permissions
     *
     * @return $this
     */
    public function syncPermissions($permissions)
    {
        $this->permissions()->detach();

        return $this->givePermissionTo($permissions);
    }

    /**
     * Revoke the given permission.
     *
     * @param $permission
     *
     * @return HasPermissions
     */
    public function revokePermissionTo($permission)
    {
        $this->permissions()->detach($this->getStoredPermission($permission));

        return $this;
    }

    /**
     * Get stored permissions and create new ones if there is so called virtual
     * permissions present virtual permission is named route that is
     * not yet stored into permissions table to database.
     *
     * @param string|array|Permission|\Illuminate\Support\Collection $permissions
     *
     * @return Permission
     */
    protected function getStoredPermission($permissions)
    {
        if (is_string($permissions)) {

            $permission = app(Permission::class)->where('name', $permissions)->first();

            if (!$permission) {
                return $this->createNewPermissionIfRouteExist($permissions);
            }

            return $permission;
        }

        // Array of permissions and some might not be created yet. So called virtual permissions.
        if (is_array($permissions)) {
            // Get all already existing permissions from database.
            $existingPermissions = app(Permission::class)->whereIn('name', $permissions)->get();

            // Get possible virtual permissions.
            $notExistingPermissions = collect($permissions)->diff($existingPermissions);

            // Create new permissions if the virtual permissions is defined in applications named routes.
            foreach ($notExistingPermissions as $route) {
                $newPermission = $this->createNewPermissionIfRouteExist($route);

                if ($newPermission) {
                    $existingPermissions->push($newPermission);
                }
            }

            return $existingPermissions;
        }

        return $permissions;
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function createNewPermissionIfRouteExist($name)
    {
        if ($this->routeExist($name)) {
            return app(Permission::class)->create(['name' => $name]);
        }

        return;
    }

    /**
     * @param $permission
     * @return bool
     */
    protected function routeExist($permission)
    {
        return (bool)$this->getRouteNames()->search($permission);
    }

    /**
     * @return static
     */
    protected function getRouteNames()
    {
        return collect(Route::getRoutes())->map(function ($route) {
            return $route->getName();
        })->reject(function ($name) {
            return empty($name);
        });
    }
}