<?php

namespace Klopal\Permissions\Models\Traits;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache as CacheFacade;

trait Cache
{
    /**
     * 20160 minutes = 14 days
     *
     * @var int
     */
    protected $time = 20160;

    /**
     * Tag all users cache keys to given user so they are easy to flush when ever there
     * are changes in user's roles.
     *
     * @param $key
     * @param $query
     * @return mixed
     */
    protected function cache($key, $query, $verb = 'get')
    {
        if (config('klopal-permissions.cache') === true) {

            return CacheFacade::tags('klopal.permissions.users.' . $this->id)->remember($key, $this->time, function () use ($query, $verb, $key) {

                $this->log('Refreshing cache for user ' . $this->id . ' and key: ' . $key);

                return $this->createQuery($query, $verb);
            });
        }

        return $this->createQuery($query, $verb);
    }

    /**
     *
     */
    public function forgetUserCache()
    {
        if (config('klopal-permissions.cache') === true) {

            CacheFacade::tags('klopal.permissions.users.' . $this->id)->flush();
            CacheFacade::forget('klopal.permissions.user.' . $this->id . '.organizations');

            $this->log('Flushing user ' . $this->id . ' all cache key: klopal.permissions.users.' . $this->id);
        }
    }

    /**
     * @param $query
     * @param $verb
     * @return mixed
     */
    protected function createQuery($query, $verb)
    {
        if ($verb == 'first') {
            return $query->first();
        }

        if ($verb == 'all') {
            return $query->all();
        }

        return $query->get();
    }

    /**
     * @param $message
     */
    protected function log($message)
    {
        if (config('app.debug')) {
            Log::info($message);
        }
    }
}