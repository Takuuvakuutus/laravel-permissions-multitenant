<?php

namespace Klopal\Permissions\Models\Traits;

use Klopal\Permissions\Models\Organization;
use Klopal\Permissions\Exceptions\OrganizationDoesNotExist;

/**
 * User may belong to many organizations. The organization user is viewing is saved to database
 * to current_organization_id -field in users table.
 *
 * Class HasOrganizations
 * @package Klopal\Permissions\Traits
 */
trait HasOrganizations
{
    use CacheOrganization;

    /**
     * How many organizations user is related to.
     *
     * @return mixed
     */
    public function hasOrganizations()
    {
        return $this->organizations->count();
    }

    /**
     * @return mixed
     */
    public function organizations()
    {
        return $this->belongsToMany(
            config('klopal-permissions.models.organization'),
            config('klopal-permissions.table_names.role_user'),
            'user_id', 'organization_id')
            ->withPivot('role_id')
            ->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function getOrganizationsAttribute()
    {
        return $this->organizations()->get()->unique('id');
    }

    /**
     * @param $organizationId
     * @return mixed
     */
    public function rolesInOrganization($organizationId)
    {
        return $this->roles()->wherePivot('organization_id', '=', $organizationId)->get();
    }

    /**
     * Determine if the user is on the given organization.
     *
     * @param $organizationId
     * @return mixed
     */
    public function onOrganization($organizationId)
    {
        return $this->organizations->contains($organizationId);
    }

    /**
     * Switch the current organization for the user.
     *
     * @param $organization
     * @throws InvalidArgumentException
     */
    public function switchToOrganization($organization)
    {
        if (!$this->onOrganization($organization)) {
            throw new \InvalidArgumentException("The user does not belong to the given organization.");
        }

        if ($organization instanceof Organization) {
            $this->current_organization_id = $organization->id;
        } elseif (is_integer($organization)) {
            $this->current_organization_id = $organization;
        } else {
            throw new \InvalidArgumentException("Organization must be either Organization object or integer.");
        }

        $this->forgetUserCache();

        $this->save();
    }

    /**
     * Refresh the current organization for the user.
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function refreshCurrentOrganization()
    {
        $this->current_organization_id = null;

        $this->save();

        $this->forgetUserCache();

        return $this->currentOrganization();
    }

    /**
     * Accessor for the currentOrganization method.
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getCurrentOrganizationAttribute()
    {
        return $this->currentOrganization();
    }

    /**
     * Get the organization that user is currently at.
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function currentOrganization()
    {
        $organizations = $this->cacheUserOrganizations(
            'klopal.permissions.user.' . $this->id . '.organizations',
            $this->organizations()
        );

        // If user is not in any organization at the moment.
        if (is_null($this->current_organization_id)) {

            // If user does not belong to any organizations.
            if (empty($organizations)) {
                return null;
            }

            $hasNullableCurrentOrganizationRole = false;
            $nullableCurrentOrganizationRoles = config('klopal-permissions.nullable_current_organization_roles');
            if(!$nullableCurrentOrganizationRoles){
                // If nullable roles are not set, then use default roles.
                $nullableCurrentOrganizationRoles = [
                    config('klopal-permissions.admin_role'),
                    'claim-handler'
                ];
            }

            foreach($nullableCurrentOrganizationRoles as $role) {
                if ($this->hasGenericRole($role)) {
                    $hasNullableCurrentOrganizationRole = true;
                }
            }

            //Admins and usually other generic roles may not have current organization set.
            if ($hasNullableCurrentOrganizationRole) {
                return null;
            } else {
                $this->switchToOrganization($organizations->first());
            }

            return $this->currentOrganization();
        } else {
            // If user can admin allow him to switch any existing organization.
            if ($this->hasGenericRole(config('klopal-permissions.admin_role'))) {
                $currentOrganization = app(Organization::class)->find($this->current_organization_id);
            } else {
                // If user can not admin and does not belong to any organization.
                if(empty($organizations)) {
                    $this->current_organization_id = null;
                    $this->save();

                    return null;
                }

                $currentOrganization = $organizations->where('id', $this->current_organization_id)->first();

                // If user does not belong to organization anymore.
                if(empty($currentOrganization)) {
                    $this->current_organization_id = null;
                    $this->save();

                    return $this->currentOrganization();
                }
            }

            return $currentOrganization ?: $this->refreshCurrentOrganization();
        }
    }

    /**
     * @param $organization
     * @return mixed
     * @throws OrganizationDoesNotExist
     */
    protected function getStoredOrganization($organization)
    {
        if (is_string($organization) or is_integer($organization)) {
            
            $organization = $this->cache('klopal.permissions.organization.' . $organization,
                    app(Organization::class)->where('id', $organization), 'first');

            if (!$organization) {
                $organization = app(Organization::class)->find($organization);
            }
            return $organization;
        }

        if ($organization instanceof Organization) {
            return $organization;
        }

        throw new OrganizationDoesNotExist("The given organization does not exist.");
    }

    /**
     * @return mixed
     */
    public function getOrganizationRolesListAttribute()
    {
        return $this->rolesInOrganization($this->currentOrganization()->id)->pluck('name')->toArray();
    }
}