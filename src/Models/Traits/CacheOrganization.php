<?php

namespace Klopal\Permissions\Models\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache as CacheFacade;

trait CacheOrganization
{
    /**
     * 20160 minutes = 14 days
     *
     * @var int
     */
    protected $time = 20160;

    /**
     * @param $key
     * @param $query
     * @return mixed
     */
    protected function cacheUserOrganizations($key, $query)
    {
        if (config('klopal-permissions.cache') === true) {

            $this->log('Fetching user organizations. User: ' . $this->id . ' and key: ' . $key);

            $organizations = CacheFacade::get($key);
            if(!empty($organizations)){
                $this->log('User organizations: ' . count($organizations));
                $this->log(CacheFacade::has($key) . ' & ' . count($organizations));
            }
            if (!CacheFacade::has($key) or empty($organizations)) {
                $organizations = $query->get()->unique('id');

                CacheFacade::put($key, $organizations, $this->time);

                $this->log('Refreshing organization cache for user ' . $this->id . ' and key: ' . $key);
            }

            return $organizations;
        }

        return $query->get();
    }
}