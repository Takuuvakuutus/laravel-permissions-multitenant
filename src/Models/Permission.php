<?php

namespace Klopal\Permissions\Models;

use Illuminate\Database\Eloquent\Model;
use Klopal\Permissions\Exceptions\PermissionDoesNotExist;
use Klopal\Permissions\Models\Traits\RefreshesPermissionCache;
use Klopal\Permissions\Contracts\Permission as PermissionContract;

class Permission extends Model implements PermissionContract
{
    // When ever permission is stored, updated, deleted we flush related cache.
    use RefreshesPermissionCache;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    public $fillable = ['name', 'description'];

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('klopal-permissions.table_names.permissions'));
    }

    /**
     * A permission can be applied to roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(
            config('klopal-permissions.models.role'),
            config('klopal-permissions.table_names.permission_role')
        )->withTimestamps();
    }

    /**
     * Find a permission by its name.
     *
     * @param string $name
     * @return mixed
     * @throws PermissionDoesNotExist
     */
    public static function findByName($name)
    {
        $permission = static::where('name', $name)->first();

        if (!$permission) {
            throw PermissionDoesNotExist::doesNotExist($name);
        }

        return $permission;
    }
}