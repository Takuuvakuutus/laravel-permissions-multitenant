<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Developers
    |--------------------------------------------------------------------------
    |
    | Developers are superusers that pass all authorizations checks.
    | Insert emails.
    |
    */

    'developers' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | Cache
    |--------------------------------------------------------------------------
    |
    | Should package use cache to store permission data. Notice, don't use
    | database or file driver because they don't support tags.
    |
    */

    'cache' => true,

    /*
   |--------------------------------------------------------------------------
   | Admin Permission
   |--------------------------------------------------------------------------
   |
   | A role name that will allow to use organizations even if she doesn't
   | have permissions to it. Basically it allows admin to
   | swap between all organizations.
   |
   */

    'admin_role' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Authorization Models
    |--------------------------------------------------------------------------
    */

    'models' => [

        /*
        |--------------------------------------------------------------------------
        | Role Model
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | Eloquent model should be used to retrieve your roles. Of course, it
        | is often just the "Role" model but you may use whatever you like.
        |
        | The model you want to use as a Role model needs to implement the
        | `Klopal\Permissions\Contracts\Role` contract.
        |
        */

        'role'         => Klopal\Permissions\Models\Role::class,

        /*
        |--------------------------------------------------------------------------
        | Permission Model
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | Eloquent model should be used to retrieve your permissions. Of course, it
        | is often just the "Permission" model but you may use whatever you like.
        |
        | The model you want to use as a Permission model needs to implement the
        | `Klopal\Permissions\Contracts\Permission` contract.
        |
        */
        'permission'   => Klopal\Permissions\Models\Permission::class,

        /*
        |--------------------------------------------------------------------------
        | Organization Model
        |--------------------------------------------------------------------------
        |
        | `Klopal\Permissions\Contracts\Organization` contract.
        |
        */
        'organization' => Klopal\Permissions\Models\Organization::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Authorization Tables
    |--------------------------------------------------------------------------
    */

    'table_names' => [

        /*
        |--------------------------------------------------------------------------
        | Users Table
        |--------------------------------------------------------------------------
        |
        | The table that your application uses for users. This table's model will
        | be using the "HasRoles" and "HasPermissions" traits.
        |
        */
        'users' => 'users',


        /*
        |--------------------------------------------------------------------------
        | Roles Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your roles. We have chosen a basic
        | default value but you may easily change it to any table you like.
        |
        */

        'roles' => 'roles',

        /*
        |--------------------------------------------------------------------------
        | User Roles Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your users roles. We have chosen a
        | basic default value but you may easily change it to any table you like.
        |
        */

        'role_user' => 'role_user',

        /*
        |--------------------------------------------------------------------------
        | Permissions Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your permissions. We have chosen a basic
        | default value but you may easily change it to any table you like.
        |
        */

        'permissions'     => 'permissions',

        /*
        |--------------------------------------------------------------------------
        | Role Permissions Table
        |--------------------------------------------------------------------------
        |
        | When using the "HasRoles" trait from this package, we need to know which
        | table should be used to retrieve your roles permissions. We have chosen a
        | basic default value but you may easily change it to any table you like.
        |
        */
        'permission_role' => 'permission_role',

        /*
        |--------------------------------------------------------------------------
        | Organizations Table
        |--------------------------------------------------------------------------
        |
        | This package is designed multi tenancy in mind so we will set a table and
        | a term for tenant.
        |
        */
        'organizations'   => 'organizations',
    ],
];
