<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('roles')->truncate();

        \Klopal\Permissions\Models\Role::create([
            'name'        => 'developer',
            'slug'        => 'Developer',
            'generic'     => true,
            'description' => 'Developer have all permissions including manage users, roles and permissions.',
        ]);
    }
}