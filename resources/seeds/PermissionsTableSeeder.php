<?php

use Illuminate\Database\Seeder;
use Klopal\Permissions\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('permissions')->truncate();
        DB::table('permission_role')->truncate();

        $developer = \Klopal\Permissions\Models\Role::where('name', '=', 'developer')->first();

        $developer->givePermissionTo([
            Permission::firstOrCreate(['name' => 'developer']),
            Permission::firstOrCreate(['name' => 'admin']),
            Permission::firstOrCreate(['name' => 'users.index']),
            Permission::firstOrCreate(['name' => 'users.create']),
            Permission::firstOrCreate(['name' => 'users.store']),
            Permission::firstOrCreate(['name' => 'users.show']),
            Permission::firstOrCreate(['name' => 'users.edit']),
            Permission::firstOrCreate(['name' => 'users.update']),
            Permission::firstOrCreate(['name' => 'users.destroy']),

            Permission::firstOrCreate(['name' => 'users.create.organization']),

            Permission::firstOrCreate(['name' => 'organizations.index']),
            Permission::firstOrCreate(['name' => 'organizations.create']),
            Permission::firstOrCreate(['name' => 'organizations.store']),
            Permission::firstOrCreate(['name' => 'organizations.show']),
            Permission::firstOrCreate(['name' => 'organizations.edit']),
            Permission::firstOrCreate(['name' => 'organizations.update']),
            Permission::firstOrCreate(['name' => 'organizations.destroy']),

            Permission::firstOrCreate(['name' => 'permissions.index']),
            Permission::firstOrCreate(['name' => 'permissions.create']),
            Permission::firstOrCreate(['name' => 'permissions.store']),
            Permission::firstOrCreate(['name' => 'permissions.show']),
            Permission::firstOrCreate(['name' => 'permissions.edit']),
            Permission::firstOrCreate(['name' => 'permissions.update']),
            Permission::firstOrCreate(['name' => 'permissions.destroy']),

            Permission::firstOrCreate(['name' => 'roles.index']),
            Permission::firstOrCreate(['name' => 'roles.create']),
            Permission::firstOrCreate(['name' => 'roles.store']),
            Permission::firstOrCreate(['name' => 'roles.show']),
            Permission::firstOrCreate(['name' => 'roles.edit']),
            Permission::firstOrCreate(['name' => 'roles.update']),
            Permission::firstOrCreate(['name' => 'roles.destroy']),

            Permission::firstOrCreate(['name' => 'users.roles.index']),
            Permission::firstOrCreate(['name' => 'users.roles.create']),
            Permission::firstOrCreate(['name' => 'users.roles.store']),
            Permission::firstOrCreate(['name' => 'users.roles.update']),

            Permission::firstOrCreate(['name' => 'users.roles.generic-roles.update']),
            Permission::firstOrCreate(['name' => 'users.roles.generic-roles.show']),
            Permission::firstOrCreate(['name' => 'users.roles.organization-roles.update']),
        ]);
    }
}