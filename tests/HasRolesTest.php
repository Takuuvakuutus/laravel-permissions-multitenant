<?php

namespace Klopal\Permissions\Tests;

use Klopal\Permissions\Contracts\Role;
use Klopal\Permissions\Exceptions\PermissionDoesNotExist;
use Klopal\Permissions\Exceptions\OrganizationDoesNotExist;
use Klopal\Permissions\Models\Permission;

class HasRolesTest extends TestCase
{
    /** @test */
    public function it_can_determine_that_the_user_does_not_have_a_role_in_organization()
    {
        auth()->login($this->getWrite(1));

        $this->assertFalse($this->testUser->hasRoleInOrganization(['write'], 2));
    }

    /** @test */
    public function it_can_determine_that_the_user_have_a_role_in_organization()
    {
        auth()->login($this->getWrite(1));

        $this->assertTrue($this->testUser->hasRoleInOrganization(['write'], 1));
    }

    /** @test */
    public function it_can_assign_and_remove_a_role()
    {
        $this->testUser->syncRoles(['write'], 1);

        $this->assertTrue($this->testUser->hasRoleInOrganization('write', 1));

        $this->testUser->removeRole('write');

        $this->refreshTestUser();

        $this->assertFalse($this->testUser->hasRole('write', 1));
    }

    /** @test */
    public function it_can_assign_multiple_roles_at_once_to_certain_organization()
    {
        $this->testUser->syncRoles(['write', 'read'], 2);

        $this->assertTrue($this->testUser->hasRole('write', 2));

        $this->assertTrue($this->testUser->hasRole('read', 2));
    }

    /** @test */
    public function it_will_remove_all_roles_when_an_empty_array_is_past_to_sync_roles()
    {
        $this->testUser->syncRoles(['write'], 1);

        $this->testUser->syncRoles(['read'], 1);

        $this->testUser->syncRoles([], 1);

        $this->assertFalse($this->testUser->hasRole('write'), 1);

        $this->assertFalse($this->testUser->hasRole('read'), 1);
    }

    /** @test */
    public function it_wont_sync_roles_if_no_organization_is_given()
    {
        $this->setExpectedException(OrganizationDoesNotExist::class);

        $this->testUser->syncRoles(['read']);
    }

    /** @test */
    public function it_wont_sync_roles_if_no_valid_organization_is_given()
    {
        $this->setExpectedException(OrganizationDoesNotExist::class);

        $this->testUser->syncRoles(['read'], 3);
    }

    /** @test */
    public function it_can_assign_a_role_using_an_object()
    {
        $this->testUser->syncRoles([$this->testRole], 1);

        $this->refreshTestUser();

        $this->assertTrue($this->testUser->hasRoleInOrganization($this->testRole, 1));
    }

    /** @test */
    public function it_can_determine_that_a_user_has_one_of_the_given_roles_organization()
    {
        $roleModel = app(Role::class);

        $this->assertFalse($this->testUser->hasRoleInOrganization($roleModel->all(), 2));

        $this->testUser->syncRoles([$this->testRole], 2);

        $this->refreshTestUser();

        $this->assertTrue($this->testUser->hasRoleInOrganization($this->testRole, 2));
    }


    /** @test */
    public function it_can_determine_that_the_user_does_not_have_a_permission()
    {
        $this->setExpectedException(PermissionDoesNotExist::class);

        $this->assertFalse($this->testUser->hasPermissionTo('articles.edit'));
    }

    /** @test */
    public function it_can_work_with_a_user_that_does_not_have_any_permissions_at_all()
    {
        $this->setExpectedException(PermissionDoesNotExist::class);

        $user = new User();

        $this->assertFalse($user->hasPermissionTo('articles.edit'));
    }

    /** @test */
    public function it_can_determine_that_the_user_does_not_have_a_permission_even_with_non_existing_permissions()
    {
        $this->setExpectedException(PermissionDoesNotExist::class);

        $this->assertFalse($this->testUser->hasPermissionTo('this permission does not exists'));
    }

    /** @test */
    public function it_can_sync_a_permissions_to_a_role()
    {
        $this->testRole->syncPermissions(['write', 'article.create']);

        $this->testUser->syncRoles([$this->testRole], 1);

        $this->assertTrue($this->testUser->hasPermissionTo('write'));
        $this->assertTrue($this->testUser->hasPermissionTo('article.create'));
        $this->assertFalse($this->testUser->hasPermissionTo('read'));

    }

    /** @test */
    public function it_will_remove_all_permission_on_a_role_when_passing_an_empty_array_to_sync_permissions()
    {
        $this->testRole->syncPermissions(['article.create']);

        $this->testRole->syncPermissions(['read']);

        $this->testRole->syncPermissions([]);

        $this->testUser->syncRoles([$this->testRole], 1);

        $this->assertFalse($this->testUser->hasPermissionTo('write'));
        $this->assertFalse($this->testUser->hasPermissionTo('article.create'));
        $this->assertFalse($this->testUser->hasPermissionTo('read'));
    }

    public function getWrite($organizationId)
    {
        $this->testUser->syncRoles(['write'], $organizationId);

        $this->refreshTestUser();

        return $this->testUser;
    }
}
