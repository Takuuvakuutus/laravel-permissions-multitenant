<?php

namespace Klopal\Permissions\Tests;

use Klopal\Permissions\Contracts\Role;
use Klopal\Permissions\Exceptions\OrganizationDoesNotExist;
use Klopal\Permissions\Exceptions\PermissionDoesNotExist;
use Klopal\Permissions\Models\Permission;

class HasGenericRolesTest extends TestCase
{
    /** @test */
    public function it_can_determine_that_the_user_have_a_generic_role_string()
    {
        auth()->login($this->getGeneric());

        $this->assertTrue($this->testUser->hasGenericRole('admin'));
    }

    /** @test */
    public function it_can_determine_that_the_user_have_a_generic_role_array()
    {
        auth()->login($this->getGeneric());

        $this->assertTrue($this->testUser->hasGenericRole(['admin']));
    }

    /** @test */
    public function it_can_determine_that_the_user_have_a_generic_role_object()
    {
        auth()->login($this->getGeneric());

        $this->assertTrue($this->testUser->hasGenericRole($this->genericRole));
    }

    /** @test */
    public function it_can_assign_and_remove_a_generic_role()
    {
        $this->testUser->syncGenericRoles(['admin']);

        $this->assertTrue($this->testUser->hasGenericRole('admin'));

        $this->testUser->syncGenericRoles([]);

        $this->refreshTestUser();

        $this->assertFalse($this->testUser->hasGenericRole('admin'));
        $this->assertFalse($this->testUser->hasGenericRole(['admin']));
        $this->assertFalse($this->testUser->hasGenericRole($this->genericRole));
    }

    /** @test */
    public function it_can_sync_generic_roles()
    {
        $this->testUser->syncGenericRoles(['admin', 'moderator']);

        $this->assertTrue($this->testUser->hasGenericRole('admin'));

        $this->assertTrue($this->testUser->hasGenericRole('moderator'));
    }

    /** @test */
    public function it_wont_sync_generic_roles_if_nongeneric_is_given()
    {
        $this->testUser->syncGenericRoles(['admin', 'write']);

        $this->assertTrue($this->testUser->hasGenericRole('admin'));
        $this->assertFalse($this->testUser->hasGenericRole('write'));
    }

    /** @test */
    public function it_will_remove_all_generic_roles_when_an_empty_array_is_past_to_sync_roles()
    {
        $this->testUser->syncGenericRoles(['admin', 'moderator']);

        $this->assertTrue($this->testUser->hasGenericRole('admin'));
        $this->assertTrue($this->testUser->hasGenericRole('moderator'));

        $this->testUser->syncGenericRoles([]);

        $this->assertFalse($this->testUser->hasGenericRole('admin'));
        $this->assertFalse($this->testUser->hasGenericRole('moderator'));
    }

    /** @test */
    public function it_can_assign_a_role_using_an_object()
    {
        $this->testUser->syncGenericRoles([$this->genericRole]);

        $this->refreshTestUser();

        $this->assertTrue($this->testUser->hasGenericRole($this->genericRole));
    }

    /** @test */
    public function it_can_determine_that_a_user_has_one_of_the_given_generic_roles()
    {
        $this->assertFalse($this->testUser->hasGenericRole($this->genericRole));
        $this->assertFalse($this->testUser->hasGenericRole('moderator'));

        $moderator = app(Role::class)->where('name', 'moderator')->first();

        $this->testUser->syncGenericRoles([$moderator->name]);

        $this->refreshTestUser();

        $this->assertTrue($this->testUser->hasGenericRole($moderator));
    }

    public function getGeneric()
    {
        $this->testUser->syncGenericRoles(['admin']);

        $this->refreshTestUser();

        return $this->testUser;
    }
}
