<?php

namespace Klopal\Permissions\Tests;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Klopal\Permissions\Models\Traits\HasRoles;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Klopal\Permissions\Models\Traits\HasPermissions;
use Klopal\Permissions\Models\Traits\HasOrganizations;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthorizableContract, AuthenticatableContract
{
    use HasOrganizations, HasRoles, HasPermissions, Authorizable, Authenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email'];

    public $timestamps = false;

    protected $table = 'users';
}
