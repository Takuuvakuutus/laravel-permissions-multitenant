<?php

namespace Klopal\Permissions\Tests;

use File;
use Klopal\Permissions\Contracts\Role;
use Illuminate\Database\Schema\Blueprint;
use Klopal\Permissions\Models\Permission;
use Klopal\Permissions\Models\Organization;
use Klopal\Permissions\PermissionsRegistrar;
use Orchestra\Testbench\TestCase as Orchestra;
use Klopal\Permissions\PermissionsServiceProvider;

abstract class TestCase extends Orchestra
{
    /**
     * @var \Klopal\Permissions\Tests\User
     */
    protected $testUser;

    /**
     * @var \Klopal\Permissions\Models\Role
     */
    protected $testRole;

    /**
     * @var \Klopal\Permissions\Models\Role
     */
    protected $genericRole;

    /**
     * @var \Klopal\Permissions\Models\Organization
     */
    protected $testOrganization;

    /**
     * @var \Klopal\Permissions\Models\Permission
     */
    protected $testPermission;

    public function setUp()
    {
        parent::setUp();

        $this->setUpDatabase($this->app);

        $this->reloadPermissions();

        $this->testUser = User::first();
        $this->testRole = app(Role::class)->first();
        $this->genericRole = app(Role::class)->where('name', 'admin')->first();
        $this->testOrganization = app(Organization::class)->first();
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     *
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
           PermissionsServiceProvider::class,
        ];
    }

    /**
     * Set up the environment.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    protected function getEnvironmentSetUp($app)
    {
        $this->initializeDirectory($this->getTempDirectory());

        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => $this->getTempDirectory().'/database.sqlite',
            'prefix' => '',
        ]);

        $app['config']->set('cache.default', 'file');
        $app['config']->set('klopal-permissions.cache', false);

        $app['config']->set('view.paths', [__DIR__.'/resources/views']);
    }

    /**
     * Set up the database.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    protected function setUpDatabase($app)
    {
        // Flush database.
        file_put_contents($this->getTempDirectory().'/database.sqlite', null);

        // Create users table.
        $app['db']->connection()->getSchemaBuilder()->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
        });

        // Create roles, permissions, role_user and permission_role tables.
        include_once __DIR__.'/../resources/migrations/create_permissions_tables.php.stub';
        (new \CreatePermissionsTables())->up();

        // Create organizations table and add organization_id to role_user table.
        include_once __DIR__.'/../resources/migrations/create_organizations_tables.php.stub';
        (new \CreateOrganizationsTables())->up();

        // Seed data:
        $organization = $app[Organization::class]->create(['name' => 'Weyland-Yutani']);
        $app[Organization::class]->create(['name' => 'Acme Oy']);

        $user = User::create(['email' => 'test@user.com']);
        $user->current_organization_id = $organization->id;
        $user->save();

        $writeRole = $app[Role::class]->create(['name' => 'write', 'slug' => 'Kirjoitus']);
        $readRole = $app[Role::class]->create(['name' => 'read', 'slug' => 'Luku']);
        $app[Role::class]->create(['name' => 'admin', 'slug' => 'Admin', 'generic' => true]);
        $app[Role::class]->create(['name' => 'moderator', 'slug' => 'Moderator', 'generic' => true]);

        $writePermission = $app[Permission::class]::create([
            'name' => 'write'
        ]);
        $readPermission = $app[Permission::class]::create([
            'name' => 'read'
        ]);
        $app[Permission::class]::create([
            'name' => 'article.create'
        ]);
        $writeRole->permissions()->attach($writePermission);
        $readRole->permissions()->attach($readPermission);
    }

    /**
     * Initialize the directory.
     *
     * @param string $directory
     */
    protected function initializeDirectory($directory)
    {
        if (File::isDirectory($directory)) {
            File::deleteDirectory($directory);
        }
        if (!File::exists($directory)) {
            File::makeDirectory($directory);
        }
    }

    /**
     * Get the temporary directory.
     *
     * @param string $suffix
     *
     * @return string
     */
    public function getTempDirectory($suffix = '')
    {
        return __DIR__.'/temp'.($suffix == '' ? '' : '/'.$suffix);
    }

    /**
     * Reload the permissions.
     *
     * @return bool
     */
    protected function reloadPermissions()
    {
        return app(PermissionsRegistrar::class)->registerPermissions();
    }

    /**
     * Refresh the testuser.
     */
    public function refreshTestUser()
    {
        $this->testUser = User::find($this->testUser->id);

        $this->testUser->forgetUserCache($this->testUser);
    }
}
