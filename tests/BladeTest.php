<?php

namespace Klopal\Permissions\Tests;

class BladeTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    /** @test */
    public function all_blade_directives_will_evaluate_falsly_when_there_is_nobody_logged_in()
    {
        $role = 'write';

        $this->assertEquals('does not have role in organization', $this->renderView('hasRole', $role));
        $this->assertEquals('does not have organizations', $this->renderView('hasOrganizations'));
    }

    /** @test */
    public function all_blade_directives_will_evaluate_falsy_when_somebody_without_roles_or_permissions_is_logged_in()
    {
        $role = 'write';

        $this->actingAs($this->testUser);

        $this->assertEquals('does not have role in organization', $this->renderView('hasRole', compact('role')));
    }

    /** @test */
    public function the_role_directive_will_evaluate_false_when_the_logged_in_user_has_the_role_but_in_different_organization()
    {
        auth()->login($this->getWrite(2));

        $this->assertEquals('does not have role in organization', $this->renderView('hasRole', ['role' => 'write']));
    }

    /** @test */
    public function the_role_directive_will_evaluate_true_when_the_logged_in_user_has_the_role()
    {
        auth()->login($this->getWrite(1));

        $this->assertEquals('has role in organization', $this->renderView('hasRole', ['role' => 'write']));
    }

    public function getWrite($organizationId)
    {
        $this->testUser->syncRoles(['write'], $organizationId);

        $this->refreshTestUser();

        return $this->testUser;
    }

    public function getRead()
    {
        $this->testUser->assignRole('read');

        $this->refreshTestUser();

        return $this->testUser;
    }

    public function renderView($view, $parameters = [])
    {
        if (is_string($view)) {
            $view = view($view)->with($parameters);
        }

        return trim((string)($view));
    }
}
